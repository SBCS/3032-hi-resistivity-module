/*
 * Cond_Main.h
 *
 * Created: 7/17/2017 07:12:48
 *  Author: Fredc
 */ 


#ifndef COND_MAIN_H_
#define COND_MAIN_H_

#include "Conductivity.h"
#include "Temperature.h"
#include "EEProm.h"
#include "Averages.h"
           
  #define  CYCLES_TO_SHOW_1V          10    
  #define  CYCLES_TO_WRITE_DEBUG      30
  #define  CYCLES_TO_SAVE_CALIBRATION 40
  #define  CYCLES_TO_NORMAL_OPS       50
  #define  CYCLES_MAXIMUM           1000
  
class Cond_Main
   {
   public:
      void Cond_Init ( );           // Initialize Conductivity
      void Cond_Execute ( );        // Execute Conductivity Routine

   private:
      EEProm       aEEProm;
      Conductivity aCond;
      Temperature  aTemp;
      dblAverage   VoutAvg;
      intAverage   C1CalAvg;        // Averages from pots
      intAverage   C2CalAvg;
      intAverage   T1CalAvg;
      intAverage   T2CalAvg;
      
      bool   Cal_Saved;             // Cal data saved?
      bool   Debug_Written;         // Debug data saved?
      //int    oscnt;                 // One second count
      //int    ttcnt;                 // timer tick count
      int    Cycle_Count;           // Cycle count
      int    probe;                 // Current probe
      int    jumpers;               // Current state of Jumpers
            
      double C1CalOffset;           // Calibration offsets
      double C2CalOffset;
      double T1CalOffset;
      double T2CalOffset;
          
      double Cfreq;
      double Tfreq;
      double Tfreq_Old;
      int    Tfreq_Count;
      double Temp_Cal_Offset;
      double Cond_Cal_Offset;
      
      double Temp_Comp_Factor;
    
      double Rest_Raw;              // Resistivity
      double Rest_Cal;
      double Rest_Comp;
        
      double Temp_Raw;              // Temperature
      double Temp_Cal;
      
      double VoutFil;               // Vout filtered
      double VoutAvg10;
      double VoutOld;
      double VoutDelta;
          
      //double Calc_Cond_Offset (int raw);
      //double Calc_Temp_Offset (int raw);
      
      void   Debug_Write (void);
      void   Set_Default_Cal (void);
   };




#endif /* COND_MAIN_H_ */