/*
 * Averages.h
 *
 * Created: 12/14/2017 14:15:10
 *  Author: Fredc
 */ 


#ifndef AVERAGES_H_
#define AVERAGES_H_


#define I_AVG_SIZE 10   /* Pot averages*/
#define D_AVG_SIZE 10   /* Vout        */
#define D_MAX_DEV 0.010

   class intAverage
      {
      public:
         int iAverage (int x);
      protected:
      private:
         int Array [I_AVG_SIZE];
   
      };
 
   class dblAverage
      {
      public:
         double dAverage (double x);
         double dFixup (double x);
         void   dReset (void);
         
      protected:
      private:
         double Array [D_AVG_SIZE] ;
      };


#endif /* AVERAGES_H_ */