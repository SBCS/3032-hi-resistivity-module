/*
 * EEProm.cpp
 *
 * Created: 5/25/2017 15:16:54
 *  Author: Fredc
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/common.h>

//#include "config.h"


#include "EEProm.h"
#define  EEPROM_H_

#define DEFAULT_GAIN_CAL 512

 
 //#define  DEFAULT_CAL 512           // Gives 1.0
 
//---------------------Lay out of EEROM---------------------------
#define EEINITCOMPLETE        0x10     
//#define BASE	               0x20     /* Base were our arrays start                     */


#define EESERIALNUMBER        0x12      /* SERIAL NUMBER for this unit (switches are below */
#define RESCAL1               0x14
#define TEMPCAL1              0x16
#define RESCAL2               0x1a
#define TEMPCAL2              0x1c




//------------------------------------------------------------------------------------------------------------
//
void EEProm::EEProm_Init (void)
{
   unsigned int temp;

   temp = ReadWordEE (EEINITCOMPLETE);
   if (temp != 0x1234)                         // if EEPROM has not been initialized
     {
      EEProm_Set_Default ( );
      WriteWordEE (EEINITCOMPLETE, 0x1234);
      }
}

//void EEProm_Set_Default (void);

//-----------------------
 void EEProm::Save_Resist_Cal (int unit, int Resist_Cal) 
    {
   switch (unit)
      {
      case 1: WriteWordEE (RESCAL1,  Resist_Cal); break;
      case 2: WriteWordEE (RESCAL2,  Resist_Cal); break;
      }
   }

 int EEProm::Get_Resist_Cal  (int unit)  
    {
    switch (unit)
       {
       case 1: return ReadWordEE (RESCAL1); break;
       case 2: return ReadWordEE (RESCAL2); break;
       }
    return DEFAULT_GAIN_CAL;
    }


void EEProm::Save_Temp_Cal (int unit, int Temp_Cal)
   {
   switch (unit)
      {
      case 1: WriteWordEE (TEMPCAL1,  Temp_Cal); break;
      case 2: WriteWordEE (TEMPCAL2,  Temp_Cal); break;
      }
   }

int EEProm::Get_Temp_Cal  (int unit)
   {
  switch (unit)
     {
     case 1: return ReadWordEE (TEMPCAL1); break;
     case 2: return ReadWordEE (TEMPCAL2); break;
     }
  return DEFAULT_GAIN_CAL;
  }


 void EEProm::EEProm_Set_Default (void)
   {
   // int i;

  
  // Save_Serial_Number  (1, 102); 		//(int sw, int SN)
   //Save_PCBA_Code      (1, 0xbb); 		//(int sw, int SN)
  // Save_PCBA_Code      (1, 0x22); 		//(int sw, int SN);
 
     Save_Resist_Cal (1, DEFAULT_GAIN_CAL);
     Save_Resist_Cal (2, DEFAULT_GAIN_CAL);
     Save_Temp_Cal   (1, DEFAULT_GAIN_CAL);
     Save_Temp_Cal   (2, DEFAULT_GAIN_CAL);


   }
//==========================================================================
  ////===========EEProm Stuff===============================================================
  ////
  //  secure char read/write to EEPROM (interrupts are disabled during write)

  void EEProm::WriteByteEE (int address, unsigned char data)
  {
     unsigned char temp;                 // local temp variable
     
     while (EECR & 0x02){}               // wait for EEPROM to be ready
     temp = SREG;                        // store status register
     cli ();									   // disable interrupts
     EEAR = address;                     // set EEPROM address
     EEDR = data;                        // set EEPROM data
     EECR |= 0x04;                       // enable EEPROM for writing
     EECR |= 0x02;                       // write to EEPROM
     SREG = temp;                        // restore status register
  }

  char EEProm::ReadByteEE (int address)
  {
     unsigned char temp, data;           // local variables
     
     while (EECR & 0x02){}               // wait for EEPROM to be ready
     temp = SREG;                        // store status register
     cli ();									   // disable interrupts
     EEAR = address;                     // set EEPROM address
     EECR |= 0x01;                       // read EEPROM
     data = EEDR;                        // get data from EEPROM
     SREG = temp;                        // restore status register
     return (data);                       // return the EEPROM data
  }
  



//---------------------------------------------------------------------------------
// Basic run time routines
// Data Written Big endian here to conform to "tradition"
//
void EEProm::WriteWordEE (int address, int data)
   {
   WriteByteEE (address,     data >> 8);
   WriteByteEE (address + 1, data & 0xff);
   }

int EEProm::ReadWordEE (int address)
   {
   return (ReadByteEE (address) << 8) + ReadByteEE (address + 1);
   }
   
long EEProm::ReadLongEE (int address)
    {
    int  data_L;
    long data_U;
   
    data_U = ReadWordEE (address);
    data_L = ReadWordEE (address + 2);
     
    return (data_U << 16) + data_L;
    }
    
void EEProm::WriteLongEE (int address, long data)
    {
    WriteWordEE (address,  data >> 16);      // Write Hi (big endian)
    WriteWordEE (address + 2, data);         // Write Lo
    }

void EEProm::WriteDoubleEE (int address, double data)
   {
   tempf = data;

   unsigned char *ptr = (unsigned char*) &tempf;
   
   WriteByteEE (address,     ptr[0]);
   WriteByteEE (address + 1, ptr[1]);
   WriteByteEE (address + 2, ptr[2]);
   WriteByteEE (address + 3, ptr[3]);
   // tempc [0] = ptr[0];      // Write bytes
   // tempc [1] = ptr[1];
   // tempc [2] = ptr[2];
   // tempc [3] = ptr[3];
   
   }

double EEProm::ReadDoubleEE  (int address)
   {
   //tempf = data;

   char *ptr = (char*) &tempf;
   
   ptr [0] = ReadByteEE (address);
   ptr [1] = ReadByteEE (address + 1);
   ptr [2] = ReadByteEE (address + 2);
   ptr [3] = ReadByteEE (address + 3);
   
   //ptr[0] = tempc [0];    // Write bytes
   //ptr[1] = tempc [1];
   //ptr[2] = tempc [2];
   //ptr[3] = tempc [3];
   
   return tempf;
   }

 