/*
 * Conductivity.h
 *
 * Created: 6/2/2017 14:25:21
 *  Author: Fredc
 */ 


#ifndef CONDUCTIVITY_H_
#define CONDUCTIVITY_H_

#define F_AVG_SIZE 5
#define F_MAX_DELTA 0.05

class Conductivity
   {
   public:
      double Get_Cond_Raw (double freq);
      double Cond_Volts (double rho);
      double Calc_Cond_Offset (int raw);
      double Average (double x);
      double Fixup (double x);
      void   Reset (void);
   private:
      double Array [F_AVG_SIZE];
   };
   
     

#endif /* CONDUCTIVITY_H_ */