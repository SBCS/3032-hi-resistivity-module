/*
 * HI-Res.cpp
 *
 * Created: 6/20/2017 10:49:44
 * Author : Fredc
 */ 



/*
 * T461Cond10.cpp
 *
 * Created: 5/25/2017 14:49:18
 * Author : Fredc
 */ 
 /*
 * Cond-Temp2.c
 *
 * Created: 5/7/2015 6:24:30 PM
 *  Author: Mark
 */

 
//
#ifndef  __AVR_ATtiny861__
#error  Not ATtiny861
#endif

#include <avr/io.h>
#include <avr/interrupt.h>
#include "Cond_Main.h"

Cond_Main Cond_main;

 int main (void)
   {
   Cond_main.Cond_Init ( );
   sei ( );

   while (true)
      {
      Cond_main.Cond_Execute ( );
      }
   }
 