/*
 * Temperature.cpp
 *
 * Created: 5/25/2017 15:13:23
 *  Author: Fredc
 */ 
//
 //**************************************************************************
 //
 //   Temperature.c
 //     This module contains all code that interacts directly
 //     with the temperature.
 //
 // 10-17-17 New calibration coefficients for new hardware
 //
 //**************************************************************************/
 //  Set a default of declaring all local variables "auto" (on stack)

 //#define  F_CPU 8000000                 /* Set 8MHZ cpu*/
 //#include <util/delay.h>
 
 #include <avr/io.h>
 #include "Temperature.h"
 #include "Util.h"
 //#include "PCBA201705.h"
 

 
 //--------------------------------------------------------------------------
 // Read temperature
 //
 //
 // Get temperature correction for rho
 //  Using factor alpha = 5.5 % / degree C
 // Note that this factor is for Resistivity
 //    (inverse of factor for conductivity)
 //   resistivity at the reference temperature.
 double Temperature::Temp_Compensation_Factor (double T)
    {
    double factor; 
    double alpha  = 0.055;          // 5.5% / deg C

    factor = 1.0 + (alpha * (T - TEMP_REFERENCE));
    factor = Util::Bracket (factor, 1.75, 0.183);
    //factor = 1.0 / factor;
    return factor;
    }

double Temperature::Get_Temp_Raw (double freq)
   {
   //double T;                  // Period
   double temp;

   //if (freq < 100.0)           // If less than 100 not oscillating
    //  return 0.0;
   
   //T = 1E6 / (double) cnt;           // Calculate period
    // temp = 0.1130 * freq - 843;        // Calculate temp
   //temp = -0.0922 * freq + 385.35;      // Calculate temp 09-17-17
   
   //temp = -0.0981 * freq + 400.43;        // Calculate temp 10-12-17
   //temp = -0.1781 * freq + 346.35;        // Calculate temp 10-26-17 
   //temp = -0.0972 * freq + 357.24;        // Calculate temp 10-27-17
   //temp = -0.10155 * (double) freq + 398.05;   // 10-27-17 avg  0.047 uF Caps IV:82
   //temp = -0.1953 * (double) freq + 365.27;    // Change to 0.1 uF Caps
   //temp = -0.2014 * (double) freq + 384.67;    // Change to 0.1 uF Caps
   //temp = -0.0997 * (double) freq + 373.2;     // New Caps V:41
   //temp = 0.1305 * (double) freq -27.2576;     // New Thermistor V:173
   temp = 0.0528 * (double) freq -1.4572;    // New Thermistor VI:4
   return temp;
   }

 double Temperature::Temp_Volts (double T)
    {                  // For 0 - 51.2 C
    //return (T / 5.0);  // 0 - 10 v 
    return (T / 25.0);
    //return (T/12.5);    // New output scaling 0-5
    }
    
 double Temperature::Average (double x)
    {
    int i;
    double sum;
    
    for (i = 1; i < T_AVG_SIZE; i++)
       {Array [i] = Array [i - 1];}
    Array [0] = x;
    sum = 0;
    for (i = 0; i < T_AVG_SIZE; i++)
       {sum += Array [i];}
    return (double) sum / (double) T_AVG_SIZE;
    }
    
 double Temperature::Calc_Temp_Offset (int raw)
    {
    double F;

        F = ((double) raw  / 10.0 - 50.0);       // Range -25 to + 25
        //F = Util::Bracket (F, 25.0, -25.0);      //
        return F;
     }
     
   double Temperature::Fixup (double x)
       {
       int i;
       double sum;
       double delta;
       double UL;
       double LL;
       
       delta = T_MAX_DELTA * x;
       UL = x + delta;
       LL = x - delta;
       
       for (i = 0; i < T_AVG_SIZE; i++)
          {Array [i] = Util::Bracket (Array [i], UL, LL);}
       
       sum = 0;
       for (i = 0; i < T_AVG_SIZE; i++)
          {sum += Array [i];}
       return sum / (double) T_AVG_SIZE;
       }

   void Temperature::Reset (void)
      {
      int i;
      
      for (i = 0; i < T_AVG_SIZE; i++)
         Array [i] = 0.0;
      }