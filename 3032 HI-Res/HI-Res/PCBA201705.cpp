  /*
  * PCBA201705.cpp
  *
  * Created: 5/25/2017 15:11:31
  *  Author: Fredc
  */ 
  #include <avr/io.h>
  #include <avr/interrupt.h>
  #define F_CPU 8000000
  #include <util/delay.h>
  #include <avr/wdt.h>
  //#include "Config.h"

  #include "util.h"
  #include "PCBA201705.h"



  static int  OneSecondTimer;        // 5ms down counter for tracking 1 second intervals
  //static bool OneSecondFlag;       // 1 = 10 sec over
  //static int  HalfSecondTimer;     // 5ms down counter for tracking 1 second intervals
  //static bool HalfSecondFlag;      // 1 = 10 sec over
  static volatile bool Timer_Flag;            // 1 second flag
  //static volatile bool Timer_Tick;            // 2 ms Tick
  //static int Poll_Flag;            // Polling Flag at 1/2 sec

  //static volatile int Cond1Fcnt;
  //static volatile int Cond2Fcnt;
//
  //static volatile int Cond1F;
  //static volatile int Cond2F;
 //
  //static volatile int Temp1Fcnt;
  //static volatile int Temp2Fcnt;
//
  //static volatile int Temp1F;
  //static volatile int Temp2F;
  
   long PCBA201705::Cond1Fcnt;
   long PCBA201705::Cond2Fcnt;
   
   long PCBA201705::Cond1F;
   long PCBA201705::Cond2F;
   
   long PCBA201705::Temp1Fcnt;
   long PCBA201705::Temp2Fcnt;
   
   long PCBA201705::Temp1F;
   long PCBA201705::Temp2F;

   int PCBA201705::PA_last;
 
  //---------------------------------------------------
  #define MPLEX_PIN 0x40      // PB6
  #define JMP1_PIN  0x20      
  #define JMP2_PIN  0x08
  
  #define PWM_BIT   0x08

  //----------------------
  #define COND1BIT  0x01   // PA 0
  #define COND2BIT  0x04   // PA 2
  #define TEMP1BIT  0x02   // PA 1
  #define TEMP2BIT  0x10   // PA 4

  #define PUD_BIT   0x40       // Pull up disable

    void PCBA201705::IO_Init ( )
       {                             // See II:139
       DDRA  = 0;                    // Port A All in!
       PORTA = JMP2_PIN; // | 0x17;             // Pull upIV:60
       
       DDRB  = PWM_BIT;              // All inputs except PWM 
       PORTB = JMP1_PIN;             // Pull up

       DIDR0 = 0xe0;    //0xf2;         // Don't forget the DIDR (II:176)
       DIDR1 = 0x10;
       }


     bool PCBA201705::Mplex_Get ( )
       {
       if ((PINB &  MPLEX_PIN) == 0) return true;  // Mplex
       return false;
       }
       
     bool PCBA201705::JUMPER_Get (int jmp)                // 1=JMP7, 2=JMP8
       {
       switch (jmp)
          {
          case 1:    
              if ((PINB & JMP1_PIN) == 0) return true;     //
              return false;
              break;
          case 2:
              if ((PINA & JMP2_PIN) == 0) return true;     // 
              return false;
              break;
          } 
       return false;             
       }
 
    void PCBA201705::Test (int t)
       {
       // Stub to inset test code
       }
    //=============================================================
    //
    void PCBA201705::Freq_Init (void)  // Initialize Freq detectors
       {
       MCUCR  |= 0b00000010;           // Falling edge III:7, IV:60
       GIMSK  |= 0b00100000;           // Enable PCIE0 (Pin Change interrupt)
      // GIFR   |= 0x20;               // Shows interrupt flag and external int enables
       PCMSK0 = 0x17;                  // PCINT 0,1,2,4 III:7 IV:60
       PCMSK1 = 0;
       
       Cond1Fcnt = 0;
       Cond2Fcnt = 0;
       Cond1F    = 0;
       Cond2F    = 0;
       Temp1Fcnt = 0;
       Temp2Fcnt = 0;
       Temp1F    = 0;
       Temp2F    = 0;
       
       PA_last = PINA; 
       }
       
    
//---------------------------------------------------------------------

    long PCBA201705::Cond_Read_Freq (int chan)         // Read Conductivity bit
       {
       long cnt;

       switch (chan)
          {
          case 1: cnt = Cond1F;  break;
          case 2: cnt = Cond2F;  break;
          default: cnt = Cond1F; break;
          }
       return cnt;
       }


//---------------------------------------------
   long PCBA201705::Temp_Read_Freq (int chan)         // Read Conductivity bit
      {
      long cnt;

      switch (chan)
         {
         case 1: cnt = Temp1F;  break;
         case 2: cnt = Temp2F;  break;
         default: cnt = Temp1F; break;
         }
      return cnt;
      }

   //---------------------------------------------------------------
   // Ref III:170  
   //#define ADC_TEMP1_CAL 4 (Defined in .h file)
   //#define ADC_COND1_CAL 5
   //#define ADC_TEMP2_CAL 6
   //#define ADC_COND2_CAL 7

     int PCBA201705::CalPot_Read (int chan)            // Get calibration factor
       {
       int    ADC_cnt;
       
       ADC_Start (chan, true);      // Temp 1 Cal  
       while  (!ADC_Done ( ))
         ;
        ADC_cnt = ADC_Read ( );
        return ADC_cnt;           
       }
      
     //
     //==============================================================
     
    #define ADPS 7                         // ADC Pre-scaler (p 160)
    
    void PCBA201705::ADC_Init ( )
       {
       ADCSRA = ADPS;               // Set pre scale to /64 SBCS  IV:196
       PRR = 0;                     // Power Reduction p DK V:45, VI:180
       ADCSRB = 0;                  // Disable Analog Compare mode (p 265)
       ADCSRA |= 0x80 | ADPS;       // Turn on  ADC
       //DIDR0  = 0xe8;               // Do the DIDR
       //DIDR1  = 0x10;
       
       }
    //---------------------ADC-----------------------------
    
    // Division Factors (0..7)                                        // 2 2 4 8 16 32 64 128
    void PCBA201705::ADC_Start (int chan, bool High_Range)           // Delete High sensitivity range
       {                                                             // SBCS IV:196
       //ADCSRA = 0x02;                     // Turn off ADC
       //ADCSRA = 0x82;                     // Turn ADC on
       while ((ADCSRA & 0x40) != 0)       // Wait till done
           ;
       //if (High_Range) 
       //   {
          ADCSRB = 0;
          ADMUX = chan & 0x07;      //1f       // Set 5 v ref was 07
        //  }          
       //   else 
       //   {
       //   ADCSRB |= 0x10; 
       //   ADMUX = (chan & 0x1f) | 0x80;   // Set 2.56v ref was 7  80               //
         // } 
        // _delay_ms (1);                 // Wait for band-gap to stabilize
        //ADCSRA = ADPS;                  // ADPS is prescale defined to be 4 
        //ADCSRA = 0xc0 | ADPS; 
        ADCSRA |= 0x40 | 0x80;                   // Start conversion
                 
       //ADCSRA |= 0x40;                  // Start Conversion
       ////ADCSRA = 0xc0 | ADPS;          // Select Channel
       }

    bool PCBA201705::ADC_Done ( )
       {
       if ((ADCSRA & 0x40) != 0)    // Check SC Bit
       //if (!(ADCSRA & 0x10))      // Check IF Bit
          return false;

       //ADCSRA |= 0x10;            // Clear IF Bit
       return true;
       }

    int PCBA201705::ADC_Read ( )
       {
       int reading;
       
       reading = 0;
       while ((ADCSRA & 0x40) != 0)              // Wait till done
          ;
       reading = ADCL & 0xff;                 // Must read L first
       reading = (ADCH << 8) | reading;       // Then H (hardware requirement)
       reading &= 0x3ff;
       // ADCSRA = ADPS;                       // ADC off
       return reading;
       }

    //---------------------PWM-----------------------------------
    void PCBA201705::PWM_Init ( )
       {
       PLLCSR = 0x02;         // Enable PLL II:172
       _delay_ms (150);       // Wait a while
       while ((PLLCSR & 1) == 0) ;     // Wait for lock

       PLLCSR = 0x06;         // PLL 84 low
       TCCR1A = 0x31;         // 21
       TCCR1B = 1;            //1
       //TCCR1C = 0x30;         // Shadow
       TCCR1D = 1;
       TCCR1E = 4;            // Output compare override //4
       

       TC1H = 0x03;         // Set top 3ff
       OCR1C = 0xff;
       TCNT1 = 0x7f;
       
       }


    void PCBA201705::PWM_Set (int pwm)
       { 
       int cnt;

       cnt = 0x3ff - (pwm & 0x3ff);
       TC1H = (cnt >> 8) & 3;
       OCR1B = cnt;
       }
       
   
   void PCBA201705::PWM_Set (double volts)
      {
      int    counts;
      double countsf;
      
      //countsf = 102.3 * volts;       // 10.0 v is full scale
      //countsf = 204.6 * volts;         // 5.0v is full scale
      countsf = 409.6 * volts;         // 2.5v is full scale
      counts = Util::Bracket ((int) countsf, 1023, 0);
      PWM_Set (counts);
      }

//-----------------------------------Watch Dog----------------------------------------------
//
void PCBA201705::WatchDog_Init (void)
   {
    
   WatchDog_Off ( );       // Turn off WDT
    return;
       
   MCUSR = 0;        // Tiny p46
   WDTCR |= 0x69;
   //wdt_enable (15);
   //wdt_enable (WDTO_8S);
   }


void PCBA201705::WatchDog_Touch (void)
   {
   // WDTCR |= 0x39;
   wdt_reset ( );
   //WDTCR |= 0x1f;
   }

void PCBA201705::WatchDog_Off  (void)
   {
   //wdt_disable ( );
   //WDTCR |= (1 << WDCE) | (1 << WDE);     // Enable turn off
   //WDTCR = 0x00;                         // Turn off WDT
   
   MCUSR &= 0x08; //_WDR();/* Clear WDRF in MCUSR */ 
   MCUSR = 0x00;            /* Write logical one to WDCE and WDE */
   WDTCR |= (1<<WDCE) | (1<<WDE);/* Turn off WDT */
   WDTCR = 0x00;
   }   

void PCBA201705::Reboot  (void)
   {
   ((void (*)(void))0)();  
   }

ISR (WDT_vect)
  {
  PCBA201705::WatchDog_Off ( );  
  PCBA201705::Reboot ( );
  }  

 //=========================================Timer============================ 

 bool PCBA201705::Get_Timer_Flag (void)
    {
    if (Timer_Flag)           // Flag is up
       {
       Timer_Flag = false;    // Lower
       return true;           //
       }
    return false;
    }

 //bool PCBA201705::Get_Timer_Tick (void)
    //{
    //if (Timer_Tick)
       //{
       //Timer_Tick = false;
       //return true;
       //}
   //return false;
    //} 
 

 #define ONESECONDTIME       500    /* Number of ticks for 1 sec    */
// #define HALFSECONDTIME      250    /* Number of ticks for 1/2 sec  */
 //#define TIMER0RELOAD        193    /* for 1 ms timer overflow 193*/
 #define TIMER0RELOAD        241    /* for 2 ms timer overflow 193*/
 void PCBA201705::Timer_Init (void)
    {
    //-------Initialize Timer-------------------------------
    cli();
    TCCR0A = 0x00;                  // 8 bit, normal
    TCCR0B = 0x05;                  // div by 5=1024 4=256 3=64 2=8 1=1 
    TIMSK |= 2;                     // enable timer overflow interrupt
    
    TCNT0H = 0;
    TCNT0L = TIMER0RELOAD;
    Timer_Flag = false;
    //Timer_Tick = false;
    OneSecondTimer = ONESECONDTIME;          // set 200*5ms for the second timer
    //HalfSecondTimer = HALFSECONDTIME;
    }

 //-----------------------------------------------------------------------------------
 ISR (TIMER0_OVF_vect)                       // Overflow detect
    {
    TCNT0H = 0;
    TCNT0L = TIMER0RELOAD;

    //Timer_Tick = true;                       //
    OneSecondTimer--;                        // Decrement the one second timer
    if (OneSecondTimer == 0)                 // If the second timer has run out ...
       {
       //PCBA201705::Cond1F = PCBA201705::Cond1Fcnt; PCBA201705::Cond1Fcnt = 0;    // Grab current counts
       //PCBA201705::Cond2F = PCBA201705::Cond2Fcnt; PCBA201705::Cond2Fcnt = 0;
       //PCBA201705::Temp1F = PCBA201705::Temp1Fcnt; PCBA201705::Temp1Fcnt = 0;
       //PCBA201705::Temp2F = PCBA201705::Temp2Fcnt; PCBA201705::Temp2Fcnt = 0;
       OneSecondTimer = ONESECONDTIME;       // Reload the one second timer
       Timer_Flag = true;                    // Show it has been one second for the check
       }
    if (OneSecondTimer < 0) OneSecondTimer = ONESECONDTIME;
    }

 ISR (PCINT_vect)
   {
   int PA_chng;
   int PA;

   PA = PINA;                                // Read current
   PA = PINA;                                // Read current
   PA = PINA;                                // Read current
   
   PA_chng = PA ^  PCBA201705::PA_last;      // Compute Changed bits
   PCBA201705::PA_last = PA;                 // Update record

   if ((PA_chng & COND1BIT) != 0) {if ((COND1BIT & PA) != 0) PCBA201705::Cond1Fcnt += 1; }
   if ((PA_chng & COND2BIT) != 0) {if ((COND2BIT & PA) != 0) PCBA201705::Cond2Fcnt += 1; }
   if ((PA_chng & TEMP1BIT) != 0) {if ((TEMP1BIT & PA) != 0) PCBA201705::Temp1Fcnt += 1; }
   if ((PA_chng & TEMP2BIT) != 0) {if ((TEMP2BIT & PA) != 0) PCBA201705::Temp2Fcnt += 1; }
   } 

   