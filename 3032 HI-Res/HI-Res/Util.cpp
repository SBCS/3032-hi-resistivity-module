/*
 * Util.cpp
 *
 * Created: 6/2/2017 11:51:15
 *  Author: Fredc
 */ 

#include "Util.h"


   int Util::Bracket (int x, int high, int low)
      {
      if (x > high) return high;
      if (x < low)  return low;
      return x;
      }

   double Util::Bracket (double x, double high, double low)
      {
      if (x > high) return high;
      if (x < low)  return low;
      return x;
      }




 //u16 round_u16 (float flt)
 //{
 //u16 rval;
 //
 //// Check for overflow
 //if ( flt > U16_MAX )
 //{
 //return U16_MAX;
 //}
 //if ( flt < U16_MIN )
 //{
 //return U16_MIN;
 //}
 //
 //rval = (u16)flt;
 //// Round off the result...
 //if (rval > 0)
 //{
 //if(flt - (float)rval > 0.4999999)
 //{
 //rval++;
 //}
 //}
 //
 //return (rval);
 //}
 //

 //s16 round_s16(float flt)
//{
  //s16 rval;
//
  //// Check for overflow
  //if ( flt > S16_MAX )
  //{
    //return S16_MAX;
  //}
  //if ( flt < S16_MIN )
  //{
    //return S16_MIN;
  //}
//
  //rval = (s16)flt;
  //// Round off the result...
  //if(rval > 0){
    //if(flt - (float)rval > 0.4999999){
      //rval++;
    //}
  //}
  //else{
    //if(flt - (float)rval < -0.4999999){
      //rval--;
    //}
  //}
//
  //return(rval);
//}
//
//#if (CAL_TYPE == 2)
//// Returns 0 on success
//// Returns <0 on failure:
////   -2 if divide-by-zero error (raw counts are the same for each point; pass in two different points)
////   -3 if offset test fails (which means there's something wrong the TwoPointCal())
//// Assuming that the relationship between readings and raw counts is linear,
//// calculate the slope and intercept of this line.
//// readings = m * (raw) + b
//// The two points needed for calibration are stored in d_calib:
//// (Gain1Raw, Gain1) and (Offset1raw, Offset1).
//s08 calculate_linear_equation(D_CAL *d_calib)
//{
  //float slope_calc, intercept_calc;
  //float intercept_test;
  //s16 intercept_calc_s16, intercept_test_s16;
//
  //// Calculate slope
  //// m = (y2-y1)/(x2-x1)
  //if (d_calib->x2 - d_calib->x1 == 0)
  //{
    //return -2;
  //}
  //slope_calc = (d_calib->y2 - d_calib->y1) /
  //(d_calib->x2 - d_calib->x1);
//
  //// Calculate y-intercept
  //// b = y-mx
  //intercept_calc = d_calib->y2 - slope_calc * d_calib->x2;  // ROUND (don't convert) this to an int, then compare
  //intercept_test = d_calib->y1 - slope_calc * d_calib->x1;
//
  //intercept_calc_s16 = round_s16(intercept_calc);
  //intercept_test_s16 = round_s16(intercept_test);
//
  //// Something went wrong with the calculation...
  //if ( intercept_calc_s16 != intercept_test_s16 )
  //{
    //return -3;
  //}
//
  //d_calib->slope = slope_calc;
  //d_calib->intercept = intercept_calc;
  //return 0;
//}
//
//// Return a calibrated reading given raw counts
//float adjust_linear(D_CAL *d_calib, float input)
//{
  //// y = mx + b
  //return d_calib->slope * input + d_calib->intercept;
//}
//#elif (CAL_TYPE == 3)
//// Construct three linear equations from three pairs of x-y coordinates
//// Stores these equations in matrix form in the array coefficients
////
//// Given three sets of values for x and y, y=ax^2+bx+c yields
//// three linear equation in three unknowns.
//// The first column stores the coefficients of a, the second those of b, etc.
//
//static void makeEquations(float x1, s16 y1, float x2, s16 y2, float x3, s16 y3, float coefficients[3][4])
//{
  //coefficients[0][0] = x1*x1;
  //coefficients[0][1] = x1;
  //coefficients[0][2] = 1;
  //coefficients[0][3] = y1;
  //coefficients[1][0] = x2*x2;
  //coefficients[1][1] = x2;
  //coefficients[1][2] = 1;
  //coefficients[1][3] = y2;
  //coefficients[2][0] = x3*x3;
  //coefficients[2][1] = x3;
  //coefficients[2][2] = 1;
  //coefficients[2][3] = y3;
//}
//// Transform the given 3x4 matrix into row-echelon form using Gaussian Elimination with partial pivoting.
//static void forwardElimination(float a[3][4])
//{
  //int i, j, k, max;     // i = row, j = column, k = pivot row
  //float m, temp;        // multiplier, temporary holding variable
  //int swap = 0;         // flag to switch lines
//
  //for (k = 0; k <= 1; k++)
  //{
    //// Partial pivot (row swapping) to avoid most division-by-zero errors
    //max = k;
    //for (i = k+1; i < 3; i++)
    //{
      //if (a[i][k] > a[max][k])
      //{
        //max = i;
        //swap = 1;
      //}
    //}
    //if (swap)
    //{
      //for (j = 0; j < 4; j++)
      //{
        //temp = a[max][j];
        //a[max][j] = a[k][j];
        //a[k][j] = temp;
      //}
      //swap = 0;
    //}
//
    //// Transform jth column into 0's
    //for (i = k+1; i <= 3-1; i++)
    //{
      //m = a[i][k] / a[k][k];
      //for (j = k; j <= 4-1; j++)
      //{
        //a[i][j] = a[i][j] - ( m*a[k][j] );
      //}
    //}
  //}
//}
//
//
//// Given a 3x4 matrix in row-echelon form, solve it using back-substitution
//static void backwardSubstitution(float a[3][4], float x[3])
//{
  //int i, j;
//
  //for (i = 3-1; i >= 0; i--)
  //{
    //x[i] = a[i][4-1];
    //for (j = i+1; j <= 3-1; j++)
    //{
      //x[i] -= a[i][j] * x[j];
    //}
    //x[i] /= a[i][i];
  //}
//}
//
//void calculate_parabolic_equation(D_CAL *d_calib)
//{
  //float coeff_matrix[3][4] = {{0}, {0}};  // Stores coefficients for system of linear equations
  //float result[3] = {0};                  // Stores coefficients for resulting parabola
//
  //// x = actual readings, y = desired readings
  //makeEquations(d_calib->x1, d_calib->y1, d_calib->x2, d_calib->y2, d_calib->x3, d_calib->y3, coeff_matrix);
  //forwardElimination(coeff_matrix);             // Transform into row-echelon form
  //backwardSubstitution(coeff_matrix, result);   // Solve
//
  //d_calib->a = result[0];
  //d_calib->b = result[1];
  //d_calib->c = result[2];
//}
//
//// Straighten non-linear (assumed parabolic) output of two-point calibrations.
//// Run calculate_parabolic_equation() before passing data into this function!
//float adjust_parabolic(D_CAL *d_calib, float input)
//{
  //// ax^2 + bx + c
  //return (d_calib->a*input*input + d_calib->b*input + d_calib->c);
//}
//#endif
//
//float factory_calib_con(float raw)
//{
  //D_CAL factory_con_calib;
//
  //// x values are raw counts
  //// y values are the desired readings after a calibration
  //// These defaults should be found with a cell constant of 1 and temperature compensation disabled
//#if (CAL_TYPE == 2)
  //factory_con_calib.x1 = condlowraw; //10 MOhm generates 35890
  //factory_con_calib.y1 = ma4raw;   //Pass ma4raw into pwm to generate 4.00 mA
  //factory_con_calib.x2 = condhiraw; //20 MOhm generates 71753
  //factory_con_calib.y2 = ma20raw;   //Pass ma20raw into pwm to generate 20.04 mA
  //factory_con_calib.slope = 0;  // These values are set by calculate_linear_equation()
  //factory_con_calib.intercept = 0;
  //calculate_linear_equation(&factory_con_calib);
  //return adjust_linear(&factory_con_calib, raw);
//#elif (CAL_TYPE == 3)
  //factory_con_calib.x1 = condlowraw;
  //factory_con_calib.y1 = ma4raw;
  //factory_con_calib.x2 = 53821.5; //No data has been taken for this point! This is a calculated value
  //factory_con_calib.y2 = 547;     //Pass 547 into pwm to generate 12.04 mA
  //factory_con_calib.x3 = condhiraw;
  //factory_con_calib.y3 = ma20raw;
  //factory_con_calib.a = 0;    // These values are set by calculate_parabolic_equation()
  //factory_con_calib.b = 0;
  //factory_con_calib.c = 0;
  //calculate_parabolic_equation(&factory_con_calib);
  //return adjust_parabolic(&factory_con_calib, raw);
//#endif
//}
//
//void limit_reading(float *reading, float low_limit, float high_limit)
//{
  //if (*reading < low_limit)
  //{
    //*reading = low_limit;
  //}
  //if (*reading > high_limit)
  //{
    //*reading = high_limit;
  //}
//}
//#ifdef _MSC_VER
//// Returns a temperature compensated conductivity reading
////10-16-14 Implement two- and three-point calibration for ORP, pH, temperature, and conductivity
//float calcTempComp_con(float reading)
//{
  //float temp_comp_factor;
  //double delta_T;
  //delta_T = 25.0172 - (float)25;
  ////delta_T = d_temp_C - (float)25;
  //temp_comp_factor = 1.0 + ( delta_T * (float)d_con_comp_val / (float)10000 );
//
  //// Apply compensation using: (compensated cond) = (uncompensated cond) / (temp_comp_factor)
  //// Remove compensation using: (uncompensated cond) = (compensated cond) * (temp_comp_factor)
  //return reading / temp_comp_factor;
//}
//#else
//// Returns a temperature compensated conductivity reading
////10-16-14 Implement two- and three-point calibration for ORP, pH, temperature, and conductivity
//float calcTempComp_con(float reading)
//{
  //float temp_comp_factor;
  //if(d_temp_C == 0){
//#ifdef DO_AVE
    //clr_ave(&temp_ave);
//#endif    
    //return reading;
  //}
  ////too much noise on temp input???
  //temp_comp_factor = 1.0 + ( (d_temp_C - (float)25) * (float)d_con_comp_val / (float)10000 );
//
  //// Apply compensation using: (compensated cond) = (uncompensated cond) / (temp_comp_factor)
  //// Remove compensation using: (uncompensated cond) = (compensated cond) * (temp_comp_factor)
  ////for resistivity invert temp comp
  ////return reading / temp_comp_factor;
  //return reading * temp_comp_factor;
//}
//#endif
//// Includes all compensations and factors. May need to separate them later.
//float raw_to_reading_con(float raw)
//{
  //float reading;
//
  //reading = factory_calib_con(raw);
  //
  //// DON'T Apply cell constant
  ////reading /= (float)d_con_cell_constp / (float)1000;
//
  //// DON'T Apply TDS factor
  ////reading *= (float)d_con_tds_fact / 1000;
//
  //// Apply temperature compensation
  //reading = calcTempComp_con(reading);
//
  //return reading;
//}
//
//u16 rd_con2( float raw, u08 chan )
//{
  //float reading,g1;
//
  //gpot[0] = AtoDstart(4);
  //gpot[1] = AtoDstart(5);
  //gpot[2] = AtoDstart(6);
  //gpot[3] = AtoDstart(7);
//
  //reading = raw_to_reading_con( raw );
//
  //// reading = user_calib( &d_con_calib, reading );
//
  //// Apply gain pot
////#ifdef DEBUG1
  ////gadc[0] = 500;
////#endif
  //g1 = get_gain(gpot[chan+1]);
  //reading *= g1;
  //limit_reading( &reading, ma4raw, ma20raw );
  //reading += get_offset(gpot[chan+0]);
//
  //
  //return round_s16(reading);
//}
//
////new temp code here
//float raw_to_reading_htr(float raw)
//{
  //float I_t;   // Thermistor current    
  //float V_t;   // Thermistor voltage    
  //float R_t;   // Thermistor resistance 
  //float T = 0; // Resulting Temperature 
  ////float A,B,CC,R;
//
  ////    T = ( 1/ ( a + b(ln R_t) + c(ln R_t)^3) ) - 273.15
  ////TODO test this with a floating-point value
  //if(raw <= 0)
  //{
    //T = 121;
  //}
  //else
  //{
    ////5-5-06 fix log error with demo
    //V_t = raw/204.6;       // Convert raw input to a voltage
    //if(V_t > 5){
      //T = 0;
      //return T;
    //}
    //I_t = (5 - V_t)/3320;     // Find current thru biasing resistor
    //R_t = V_t/I_t;        // Ohm's law
    //// Figure the constants, & calc
//
    ////from bhoward@betatherm.com
    ///*
    //A = 1.12914E-03;
    //B = 2.34126E-04;
    //CC = 8.76656E-08;
    //R = R_t;
    //T = 1/(A+B*log(R)+ CC*pow(log(R),3));
    //*/
///*
    //T = 1/( 0.0011303 + (0.0002339*log(R_t)) + (8.863E-8*pow(log(R_t), 3)));
    //// Convert from degrees Kelvin to Celsius
    //T -= 273.15;
  //}
//
  //return T;
//}
//*/
//float rd_htr2( float raw )
//{
  //float reading;
//#ifdef DO_AVE
  //save_aveq(&temp_ave, raw);
  //reading = get_aveq(&temp_ave);
  //raw = round_u16(reading);
//#endif  
  ////reading = raw_to_reading_htr(raw);
  //// reading = user_calib( &d_htr_calib, reading );
  //// Possible calibration from potentiometer
//
  ////TODO test this
 //// limit_reading( &reading, 0, 121);   // In degrees Celsius
//
  //return reading;
//}
//
////#ifndef _MSC_VER
////int main(void)
////#else
////int main3(void)
////#endif
////{
  //u08 k;
  //u16 cond1,cond2;
//#ifdef DO_AVE 
  //float condf1;//,condf2;
//#endif
//
  ////init_devices();
//
  //#ifdef DEBUG1
  //sbi(TEST_DDR,TEST0);
  //sbi(TEST_DDR,TEST1);
  //t0_off();
  //t0_on();
  //t0_off();
  //t1_off();
  //t1_on();
  //t1_off();
  //WDR();
//
  //#endif
//
  ////2-19-05 add pwm
  ////gpwm = 0;
  //for(k = 0;k<2;k++){
//#ifdef DEBUG1
    ////gpwm_val[k] = ma20raw; //20.04ma = 990
    ////gpwm_val[k] = 547; //12.04ma
   //// gpwm_val[k] = ma4raw; //4.00ma = 182
//#else 
   //// gpwm_val[k] = 0;
   //// gpwm_val[k] = ma4raw; //4.00ma
//#endif
  //}
//
//#ifdef DO_AVE
  //clr_ave(&temp_ave);
//#endif
//
  //cond1 = 0;
  //cond2 = 0;
  //d_temp_C = 0;
  //gcondstate = PINA;
//
////top:
  ////gadc[TEMP1] = AtoDstart(1);
  ////gadc[TEMP2] = AtoDstart(3);
//
  ////PORTA &= ~(1<<6); //PORTA.6 test output
  ////bot2:
//
  ////if(gcondrdy[0]){
//////#ifdef DEBUG1
    ////////gcond = 1800;
    //////d_temp_C = 770;
//////#endif
    ////d_temp_C = rd_htr2((float)gadc[TEMP1]);
    //////TODO make sure you get the correct raw counts/counter value
    //////TODO where is this value used?
////#ifdef DO_AVE    
    ////condf1 = get_aveq(&cond_ave);
    ////cond1 = rd_con2(condf1,0);
////#else
    ////cond1 = rd_con2((float)gcond[0],0);
////#endif
 //////   gpwm_val[0] = cond1;      
//////    gcondrdy[0] = 0;
  //////}
  ////
  ////if(gcondrdy[1]){   
     ////d_temp_C = rd_htr2((float)gadc[TEMP1]);
////#ifdef DO_AVE    
    ////condf1 = get_aveq(&cond_ave2);
    ////cond2 = rd_con2(condf1,2);
////#else
     ////cond2 = rd_con2((float)gcond[1],2);
////#endif
     ////gpwm_val[1] = cond2;     
     ////gcondrdy[1] = 0;
  ////} 
//
  ////do_spi(); //128 talks every 20msec
//
  ////#ifdef DEBUG1
  //////t1_tgl();
  ////#endif
//
 //// goto top;
////}
//

////float save_aveq(D_IN_AVE *p_ave, u16 val)
//
//void save_aveq(D_IN_AVE *p_ave, u16 val)
//{
////s16 n1, n2, last;
//////if 2 readings in a row within 10% then don't limit range below
////last = p_ave->Last;
////p_ave->Last = val;
////if(p_ave->Avecnt)
////{
////  if((val < ((last * 1.1) + 3)) && //within range don't limit
////  (val > ((last * .9) - 3)))
////  {
////    goto sa2; //don't limit range
////  }
////}
//////limit range here
////if(t_val > 1)
////{
////  n1 = (t_val * .9);
////  n2 = (t_val * 1.1);
////  if(val < n1)
////  val = n1;
////  if(val > n2)
////  val = n2;
////}
////sa2:
//p_ave->Ave[p_ave->Aveptr] = val;
//p_ave->Aveptr += 1;
//if(p_ave->Aveptr > 9)
//p_ave->Aveptr = 0;
//if(p_ave->Avecnt < 10){
//p_ave->Avecnt += 1;
//}
////return(get_aveq(p_ave));
//}
//#endif
//
//// Consider reimplementing when we add potentiometer calibration
////D_CAL       d_con_calib;
////D_CAL       d_con_calib2;
////D_CAL       d_htr_calib;
//
//#define pwm_on1() (PORTB |= (1<<3))
//#define pwm_off1() (PORTB &= ~(1<<3))
//#define pwm_on2() (PORTA |= (1<<3))
//#define pwm_off2() (PORTA &= ~(1<<3))
//

//#define stepscnt 200
//
//u08 get_step(u16 raw)
//{
//float sval;
//u08 i;
//sval = 0;
//for(i=0;i<stepscnt;i++){
//if(raw > sval){
//sval += (1023/stepscnt);
//}
//else{
//break;
//}
//}
//return(i);
//}
//

//float get_gain(u16 raw)
//{
//float fg;
//u08 l1;
//l1 = get_step(raw);
//if(l1==(stepscnt/2)){
//fg = 1.0;
//return(fg);
//}
////gain range adj. from 0.5 to 1.5 in 200 steps
//if(l1>=(stepscnt/2)){
//fg = 1.0 + (1.0/stepscnt) * (l1-(stepscnt/2));
//}
//else{
//fg = 0.5 + (1.0/stepscnt) * l1;
//}
//return(fg);
//}
//
//s16 get_offset (u16 raw)
//{
//u08 l1;
//s16 rval;
//l1 = get_step(raw);
//rval = l1 - (stepscnt/2);
////offset range adj. from -100 to 100 in 200 steps
//return(rval);
//}
//