/*
 * Debug.cpp
 *
 * Created: 10/19/2017 13:28:27
 *  Author: Fredc
 */ 

 #include "Cond_Main.h"
 #include "EEProm.h"
 //#include "Util.h"
 
 #define  DEBUG_AREA                 0x0040

  void Cond_Main::Debug_Write (void)
     {
     int V;
     int Power_Cycles;

     aEEProm.WriteWordEE (DEBUG_AREA, 0x1234);    //
     aEEProm.WriteWordEE (DEBUG_AREA + 0x002, probe);
     aEEProm.WriteWordEE (DEBUG_AREA + 0x004, jumpers);
     aEEProm.WriteWordEE (DEBUG_AREA + 0x006, (int) Cycle_Count);
     
     Power_Cycles = aEEProm.ReadWordEE (DEBUG_AREA + 0x008); 
     Power_Cycles += 1;
     aEEProm.WriteWordEE (DEBUG_AREA + 0x008,  Power_Cycles);
     
                                           aEEProm.WriteWordEE (DEBUG_AREA + 0x010, Cfreq);
     V = (int) (100.0 * Cond_Cal_Offset);  aEEProm.WriteWordEE (DEBUG_AREA + 0x012, V);
     V = (int) (100.0 * Rest_Raw);         aEEProm.WriteWordEE (DEBUG_AREA + 0x014, V);
     V = (int) (100.0 * Rest_Cal);         aEEProm.WriteWordEE (DEBUG_AREA + 0x016, V);
     V = (int) (100.0 * Rest_Comp);        aEEProm.WriteWordEE (DEBUG_AREA + 0x018, V);
     V = (int) (100.0 * VoutFil);          aEEProm.WriteWordEE (DEBUG_AREA + 0x01a, V);
      
                                           aEEProm.WriteWordEE (DEBUG_AREA + 0x020, Tfreq);
     V = (int) (100.0 * Temp_Cal_Offset);  aEEProm.WriteWordEE (DEBUG_AREA + 0x022, V);
     V = (int) (100.0 * Temp_Raw);         aEEProm.WriteWordEE (DEBUG_AREA + 0x024, V);
     V = (int) (100.0 * Temp_Cal);         aEEProm.WriteWordEE (DEBUG_AREA + 0x026, V);
     V = (int) (100.0 * Temp_Comp_Factor); aEEProm.WriteWordEE (DEBUG_AREA + 0x028, V);
     

     Debug_Written = true;
     }