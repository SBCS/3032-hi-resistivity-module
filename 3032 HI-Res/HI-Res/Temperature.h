/*
 * Temperature.h
 *
 * Created: 5/25/2017 15:13:06
 *  Author: Fredc
 */ 


#ifndef TEMPERATURE_H_
#define TEMPERATURE_H_

#define TEMP_REFERENCE 25.0
#define T_AVG_SIZE 5 
#define T_MAX_DELTA 0.05 
 
 
class Temperature
   {
      
   public:
      
      double Get_Temp_Raw (double freq);
      double Temp_Volts (double T);
      double Calc_Temp_Offset (int raw);
      double Average (double x);
      double Fixup (double x);
      void   Reset (void);
      
      double Temp_Compensation_Factor (double T);
      
   private:
      double Array [T_AVG_SIZE] ;
   };




#endif /* TEMPERATURE_H_ */