/*
 * Cond_Main.cpp
 *
 * Created: 7/17/2017 07:12:32
 *  Author: Fredc
 */ 
    #include "Cond_Main.h"

    #include "PCBA201705.h"
    #include "Util.h"
    
    void Cond_Main::Cond_Init ( )
       { 
       PCBA201705::IO_Init ( );           // Initialize IO
       PCBA201705::Freq_Init ( );         // Initialize Conductivity
       PCBA201705::ADC_Init ( );          // Initialize ADC
       PCBA201705::PWM_Init ( );          // Initialize PWM
       PCBA201705::Timer_Init ( );        // Initialize Timers
       PCBA201705::ADC_Start (3, true);   // Start up ADC
       PCBA201705::WatchDog_Init ( );     // Start Watchdog
       aEEProm.EEProm_Init ( );           // Initialize EE Prom
             
       //oscnt = 0;                       // One second count
       //ttcnt = 0;                       // Timer tick
       Cycle_Count = 0;                   // Cycle count
       Cal_Saved = false;                 // Calibration saved?
       Debug_Written = false;             // Debug data written?
       aCond.Reset ( );                   // Reset Conductivity averages
       aTemp.Reset ( );                   // Reset temperature averages
       VoutAvg.dReset ( );                // Reset vout averages
       VoutFil = 0.0;                     // Reset Vout filter
       VoutAvg10 = 0.0;                   // Reset Vout average
       VoutOld = 0.0;
       Tfreq_Old = 0.0;
       Tfreq_Count = 4;
       }


     void Cond_Main::Cond_Execute (void)
        {
        bool osf;             // One second flag
        //bool ttf;           // Timer tick flag
        bool J1;
        bool J2;
        double Vout;
        
        osf = PCBA201705::Get_Timer_Flag ( );   // One second flag
        // ttf = PCBA201705::Get_Timer_Tick ( );   // Timer tick
  
        if (osf)
           {  
           //oscnt += 1;
           //oscnt &= 0x3fff;  
                  PCBA201705::Cond1F = PCBA201705::Cond1Fcnt; PCBA201705::Cond1Fcnt = 0;    // Grab current counts
                  PCBA201705::Cond2F = PCBA201705::Cond2Fcnt; PCBA201705::Cond2Fcnt = 0;
                  PCBA201705::Temp1F = PCBA201705::Temp1Fcnt; PCBA201705::Temp1Fcnt = 0;
                  PCBA201705::Temp2F = PCBA201705::Temp2Fcnt; PCBA201705::Temp2Fcnt = 0;
           
           J1 = PCBA201705::JUMPER_Get (1);       // Read the jumper
           J2 = PCBA201705::JUMPER_Get (2);       // Read the jumper
           jumpers = 0;
           if (J1) jumpers += 1;
           if (J2) jumpers += 2;
           
           if (PCBA201705::Mplex_Get ( )) probe = 1; else probe = 2;    // Reverse Probe for release
           //if (PCBA201705::Mplex_Get ( )) probe = 2; else probe = 1;    // Reverse probe for test
           
           //if (jumpers != 0)                 // Get, Read, and average pots if in setup mode
           //   {
              //C1CalOffset = aCond.Calc_Cond_Offset (C1CalAvg.iAverage (PCBA201705::CalPot_Read (ADC_COND1_CAL))); 
              //C2CalOffset = aCond.Calc_Cond_Offset (C2CalAvg.iAverage (PCBA201705::CalPot_Read (ADC_COND2_CAL)));
              //T1CalOffset = aTemp.Calc_Temp_Offset (T1CalAvg.iAverage (PCBA201705::CalPot_Read (ADC_TEMP1_CAL)));
              //T2CalOffset = aTemp.Calc_Temp_Offset (T2CalAvg.iAverage (PCBA201705::CalPot_Read (ADC_TEMP2_CAL)));
           //   }

            if (probe == 1)
             { 
             //if ((jumpers == 1) || (jumpers == 2))
                //{
                C1CalOffset = aCond.Calc_Cond_Offset (C1CalAvg.iAverage (PCBA201705::CalPot_Read (ADC_COND1_CAL)));
                T1CalOffset = aTemp.Calc_Temp_Offset (T1CalAvg.iAverage (PCBA201705::CalPot_Read (ADC_TEMP1_CAL)));
                //T1CalOffset = aTemp.Calc_Temp_Offset (PCBA201705::CalPot_Read (ADC_TEMP1_CAL));
                
                Cond_Cal_Offset = C1CalOffset; //Calc_Cal_Factor (PCBA201705::CalFac_Read (ADC_COND1_CAL));
                Temp_Cal_Offset = T1CalOffset; //Calc_Cal_Factor (PCBA201705::CalFac_Read (ADC_TEMP1_CAL));
               // }
               // else
              //  {
              //  Cond_Cal_Offset = aCond.Calc_Cond_Offset (aEEProm.Get_Resist_Cal (1));
              //  Temp_Cal_Offset = aTemp.Calc_Temp_Offset (aEEProm.Get_Temp_Cal (1));
               // }  
      
             Cfreq = PCBA201705::Cond_Read_Freq (1);          // Get Avg freqs probe 1
             //Tfreq = aTemp.Average (PCBA201705::Temp_Read_Freq (1));
             Tfreq = PCBA201705::Temp_Read_Freq (1);
             }

         if (probe == 2)           // True if 24v applied
             {  
            // if ((jumpers == 1) || (jumpers == 2))                      // If 1 or 2 get from the pots
                {
                C2CalOffset = aCond.Calc_Cond_Offset (C2CalAvg.iAverage (PCBA201705::CalPot_Read (ADC_COND2_CAL)));
                T2CalOffset = aTemp.Calc_Temp_Offset (T2CalAvg.iAverage (PCBA201705::CalPot_Read (ADC_TEMP2_CAL)));
                
                Cond_Cal_Offset = C2CalOffset; // Calc_Cal_Factor (PCBA201705::CalFac_Read (ADC_COND2_CAL));
                Temp_Cal_Offset = T2CalOffset; //Calc_Cal_Factor (PCBA201705::CalFac_Read (ADC_TEMP2_CAL)); 
                }
                //else // Must be jumpers = 0 or 3
               // {
               // Cond_Cal_Offset = aCond.Calc_Cond_Offset (aEEProm.Get_Resist_Cal (2));
               // Temp_Cal_Offset = aTemp.Calc_Temp_Offset (aEEProm.Get_Temp_Cal (2));  
               // }
       
             Cfreq = PCBA201705::Cond_Read_Freq (2);          // Get Avg freqs probe 2
             //Tfreq = aTemp.Average (PCBA201705::Temp_Read_Freq (2));
             Tfreq = PCBA201705::Temp_Read_Freq (2);
             }
         //---------------------------------------------------------------------------------
         
         Vout = 0;
         //Cfreq = aCond.Fixup (Cfreq);
         //Tfreq = aTemp.Fixup (Tfreq);
         //Cfreq = aCond.Average (Cfreq);
         //Tfreq = aTemp.Average (Tfreq);
         
         //if ((Tfreq < (Tfreq_Old / 2)) || (Tfreq > (Tfreq_Old + Tfreq_Old / 2)))  // Are we out of line?
            //{                          // Yes
            //if (Tfreq_Count > 3)       // Is the count over?
               //{                       // Yes
               //Tfreq_Old = Tfreq;      // Way over
               //Tfreq_Count = 0;        // Reset the count
               //}
               //else                    //
               //{
               //Tfreq = Tfreq_Old;      // use old count
               //Tfreq_Count += 1;  
               //}
            //}            
            //else                       // We're goo
            //{                          // Average it in
            //Tfreq = aTemp.Average (Tfreq);    
            //Tfreq_Old = Tfreq;         // Update old count
            //Tfreq_Count = 0;           // Reset counter
            //}   

         switch (jumpers)
            {
            case 0:                                   // Normal Operations      
               Rest_Raw = aCond.Get_Cond_Raw (Cfreq); // Compute resistivity 
               Rest_Cal = Rest_Raw + Cond_Cal_Offset; // Calibrate Resistivity
               
               Temp_Raw = aTemp.Get_Temp_Raw (Tfreq); // Compute Temperature
               
               Temp_Cal = Temp_Raw + Temp_Cal_Offset; // Calibrate
               Temp_Comp_Factor = aTemp.Temp_Compensation_Factor (Temp_Cal); // Compensate
               
               Rest_Comp = Rest_Cal * Temp_Comp_Factor;  // Compensate Resistivity
               Vout = aCond. Cond_Volts (Rest_Comp);     // Convert to volts
               break;

            case 1:                                // Output switched to temperature
               Temp_Raw = aTemp.Get_Temp_Raw (Tfreq);      // Compute Temperature
               Temp_Cal = Temp_Raw + Temp_Cal_Offset; 
               Vout = aTemp.Temp_Volts (Temp_Cal);    // Convert to volts
               
               //Vout = aTemp.Temp_Volts (Temp_Cal_Offset); 
               break;
               
            case 2:                             // Resistivity Cal (No temp comp)
               Rest_Raw = aCond.Get_Cond_Raw (Cfreq);      // Compute resistivity
               Rest_Cal = Rest_Raw + Cond_Cal_Offset;            // Calibrate Resistivity
               Vout = aCond.Cond_Volts (Rest_Cal);  // Convert to volts
               //Vout = freq / 1000.0;
               break;
               
            case 3:                             // Debug
              if (!Cal_Saved)
                 {              
                 //if (Cycle_Count == CYCLES_TO_SAVE_CALIBRATION)
                    //{
                    ////Save_Calibration ( );
                    //aEEProm.Save_Resist_Cal (1, C1CalAvg.iAverage (PCBA201705::CalPot_Read (ADC_COND1_CAL)));
                    //aEEProm.Save_Resist_Cal (2, C2CalAvg.iAverage (PCBA201705::CalPot_Read (ADC_COND2_CAL)));
                    //aEEProm.Save_Temp_Cal   (1, T1CalAvg.iAverage (PCBA201705::CalPot_Read (ADC_TEMP1_CAL)));
                    //aEEProm.Save_Temp_Cal   (2, T2CalAvg.iAverage (PCBA201705::CalPot_Read (ADC_TEMP2_CAL)));
                    //Cal_Saved = true; 
                    //}                    
                 }
               if (Cycle_Count < CYCLES_TO_SHOW_1V) Vout = 0.5;  
               if (Cycle_Count > CYCLES_TO_SAVE_CALIBRATION) Vout = 2.0;
               //if (Cycle_Count > CYCLES_TO_NORMAL_OPS) Vout = 2.0;
                  
              //Vout = Vout * PCBA201705::CalFac_Read (ADC_TEMP2_CAL);
              //Vout = (double) Cfreq / 1000.0;
              break;
               
            default: 
               //Vout = 0.0;  
               break;
            } // End jumper switch
 

           //if (oscnt < 30)  
          PCBA201705::WatchDog_Touch ( ); 
          
          //switch (jumpers)    // Trouble shooting area
          //   {
             //case 0: Vout = 0.5;  break;
             //case 1: Vout = 1.0;  break;
             //case 2: Vout = 1.5;  break;
             //case 3: Vout = 2.0;  break;
             //   if (PCBA201705::Mplex_Get ( )) Vout = 10.0; 
             //       else  Vout = 5.0;
             //    break;
             //default: Vout = 6.0; break;
         //    }
             
          if (!Debug_Written)
             {
             if (Cycle_Count == CYCLES_TO_WRITE_DEBUG)
                {
                Debug_Write ( );
                Debug_Written = true;   
                }
             }                  
             
        //Vout = Util::Bracket (Vout, 10.0, 0.0);             // Bracket
        //VoutFil = (3.0 * VoutFil + Vout) / 4.0;            // Filter output 
                                     // Rho & Temp both filtered skip here

         VoutAvg10 = VoutAvg.dAverage (Vout);               // Boxcar average
                
         //VoutAvg10 = VoutAvg.dFixup (VoutAvg10);       
         
         VoutFil = (VoutAvg10 + 4.0 * VoutFil) / 5.0;         // Exponential smooth
         //VoutFil = VoutAvg10;
         
         //VoutDelta = (VoutFil - VoutOld) / VoutOld;
         //if (VoutDelta < 0) VoutDelta = - VoutDelta;
         //if (VoutDelta > 0.0005)
        //    {
            PCBA201705::PWM_Set (Vout);
            //PCBA201705::PWM_Set (VoutAvg10);
            //PCBA201705::PWM_Set (VoutFil);
        //    VoutOld = VoutFil;   
        //    }
                       
         
        
         
         // PCBA201705::PWM_Set (Vout);                       // Output Voltage

         if (Cycle_Count < CYCLES_MAXIMUM) Cycle_Count += 1;                         // Just let it wrap around
         //Cycle_Count &= 0x3fff;
  
          }  // end if osf--------------------------------------
                        
        //if (ttf) 
        //   { 
           //PCBA201705::Test (ttcnt);
           //ttcnt += 1;
             //if (~J1)
                //{
         //  b = ((ttcnt & 1) == 0);
           //PCBA201705::Mplex_Set (b);
                ////PCBA201705::PWM_Set (ADC_cnt);
          // }           
       }
       
 