/*
 * Util.h
 *
 * Created: 6/2/2017 11:51:31
 *  Author: Fredc
 */ 


#ifndef UTIL_H_
#define UTIL_H_

class Util
   {
   public:
      int    static Bracket (int x, int high, int low);
      double static Bracket (double x, double high, double low);

   protected:

   private:

   };



#endif /* UTIL_H_ */