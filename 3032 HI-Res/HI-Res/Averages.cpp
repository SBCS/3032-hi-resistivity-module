/*
 * Averages.cpp
 *
 * Created: 12/14/2017 14:14:09
 *  Author: Fredc
 */ 
#include "Averages.h"
#include "Util.h"

 int intAverage::iAverage (int x)
    {
    int i;
    int sum;
    
    for (i = 1; i < I_AVG_SIZE; i++)
       {
       Array [i] = Array [i - 1];
       }
    Array [0] = x;
    sum = 0;
    for (i = 0; i < I_AVG_SIZE; i++)
       {
       sum += Array [i];
       }
    return sum / I_AVG_SIZE;
    }
 

double dblAverage::dAverage (double x)
   {
   int i;
   double sum;
   
   for (i = 1; i < D_AVG_SIZE; i++)
      {Array [i] = Array [i - 1];}
   Array [0] = x;
   sum = 0;
   for (i = 0; i < D_AVG_SIZE; i++)
      {sum += Array [i];}
   return sum / D_AVG_SIZE;
   }
  

double dblAverage::dFixup (double x)
   {
   int i;
   double sum;
   double delta;
   double UL;
   double LL;
   
   delta = D_MAX_DEV * x;
   UL = x + delta;
   LL = x - delta;
   
   for (i = 0; i < D_AVG_SIZE; i++)
      {Array [i] = Util::Bracket (Array [i], UL, LL);}
         
   sum = 0;
   for (i = 0; i < D_AVG_SIZE; i++)
      {sum += Array [i];}
   return sum / (double) D_AVG_SIZE;
   }
   
   void  dblAverage::dReset (void)
      {
      int i;
         
      for (i = 0; i < D_AVG_SIZE; i++)
         Array [i] = 0.0;
      }