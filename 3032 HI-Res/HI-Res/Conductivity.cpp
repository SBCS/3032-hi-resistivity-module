   /*
    * Conductivity.cpp
    *
    * Created: 6/2/2017 14:24:36
    *  Author: Fredc
    */ 
    #include "Conductivity.h"
    #include "Util.h"
 
        
   //double Conductivity::Calculate_Cond (int cnt)
      //{
      //double T;               // Period
      //double f;               // freq kHz
      //double rho;
//
      ////if (cnt < 100.0)         // If less that 100 not oscillating
      ////   return 0.0;
      //f = (double) cnt;
      //if (f > 0.1) T = 1.0 / f;
         //else return 0;
         //
      //rho = 0.0112 * T - 1.897;          // Best linear 10-22-17
      //
      //return rho;    
      //}
      
  double Conductivity::Get_Cond_Raw (double freq)
     {
     double rhoRaw;
     double f;
    // double T;
     //
     f = (double) freq;
    // if (freq > 1) T = 1000.0 / f;         // Convert to ms
     //else return 0.0;
     
     //rhoRaw = 11.18 * T - 1.897;          // Best linear 10-22-17 IV:64
     rhoRaw = -0.0259 * f + 30.896;          // Best linear 10-22-17
     
     //rhoRaw = Calculate_Cond (freq);       // Calculate conductivity
     if (rhoRaw < 0.0) return 0.0;
     return rhoRaw;
     }    
      
   double Conductivity::Cond_Volts (double rho)
      {                             // Convert 0 to 20 Meg to 0 to 2 v
      double Vo;
 
      Vo = rho / 10.0;       // Vo = mx + b      // Calculate Vo value
      //Vo = Util::Bracket (Vo, 2.0, 0.0);
      //Vo = rho/ 5.0;       // New output scaling 0-5
      return Vo;
      }
      
      double Conductivity::Calc_Cond_Offset (int raw)
      {
         double F;

         F = ((double) raw  / 100.0 - 5.0);     // Range +- 5
         //F = Util::Bracket (F, 5.0, -5.0);      //
         return F;
      }
   
   double Conductivity::Average (double x)
      {
      int i;
      double sum;
      
      for (i = 1; i < F_AVG_SIZE; i++)
         {Array [i] = Array [i - 1];}
      Array [0] = x;
      sum = 0;
      for (i = 0; i < F_AVG_SIZE; i++)
         {sum += Array [i];}
      
      return (double) sum / (double) F_AVG_SIZE;
      }
      
   double Conductivity::Fixup (double x)
     {
     int i;
     double sum;
     double delta;
     double UL;
     double LL;
     
     delta = F_MAX_DELTA * x;
     UL = x + delta;
     LL = x - delta;
     
     for (i = 0; i < F_AVG_SIZE; i++)
        {Array [i] = Util::Bracket (Array [i], UL, LL);}
     
     sum = 0;
     for (i = 0; i < F_AVG_SIZE; i++)
        {sum += Array [i];}
     return sum / (double) F_AVG_SIZE;
     }

   void Conductivity::Reset (void)
      {
      int i;
      
      for (i = 0; i < F_AVG_SIZE; i++)
         Array [i] = 0.0;
      }
   
 