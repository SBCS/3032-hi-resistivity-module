/*
 * PCBA201705.h
 *   All these routines are specific to this PCB
 *
 * Created: 5/25/2017 15:11:53
 *  Author: Fredc
 */ 


#ifndef PCBA201705_H_
#define PCBA201705_H_

// Channels for cal constants
// Ref III:170

#define ADC_COND1_CAL 4
#define ADC_TEMP1_CAL 5

#define ADC_TEMP2_CAL 6
#define ADC_COND2_CAL 7
  
class PCBA201705 // :  public IO

   {
   public:
      static void IO_Init (void);                     // Initialize IO 

      static void Freq_Init (void);                   // Initialize Cond and Temp Freqs
      static long Cond_Read_Freq (int chan);          // Read Conductivity Freq
      static long Temp_Read_Freq (int chan);          // Read Conductivity Freq
      
      static void PWM_Init (void);                    // Initialize IO  
      static void PWM_Set (int pwm);                  //
      static void PWM_Set (double volts);             //

      static void ADC_Init (void);                    // Initialize IO
      static void ADC_Start (int chan, bool High_Range);
       
      static int  CalPot_Read (int chan);           // Get calibration pot count

      static void Timer_Init (void);                  // Initialize timer
      static bool Get_Timer_Flag (void);              // One second Timer
      //static bool Get_Timer_Tick (void);              // 2 ms Tick

      static bool JUMPER_Get (int jmp);                // 1=JMP7, 2=JMP8
      
      static bool Mplex_Get ( );                      // True if 24v applied
     
      static void WatchDog_Init  (void);
      static void WatchDog_Touch (void);
      static void WatchDog_Off   (void);
     
      static void Test (int t);                       // A test

      static void Reboot  (void);
      
      
      static int  PA_last;
      static long Cond1Fcnt;
      static long Cond2Fcnt;
      
      static long Cond1F;
      static long Cond2F;
      
      static long Temp1Fcnt;
      static long Temp2Fcnt;
      
      static long Temp1F;
      static long Temp2F;
      
   protected: 

   private:
     static bool ADC_Done ( );
     static int  ADC_Read ( );    
       
   } ;
     




#endif /* PCBA201705_H_ */